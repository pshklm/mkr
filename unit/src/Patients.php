<?php

namespace Patients;

class Patients
{
    protected $name;
    protected $patient_id;
    protected $blood_id;
    protected $resus;
    protected $PatientList = array();

    public function addPatinets($name, $patient_id, $blood_id, $resus)
    {
        return $this->DataValidator($name, $patient_id, $blood_id, $resus);
    }

    private function DataValidator($patient_id, $blood_id, $resus)
    {
        if (isset($patient_id) and $patient_id >= 0) {
            if (is_string($blood_id) and strlen($blood_id) == 1 and is_numeric($blood_id)) {
                if (is_char($resus)) {
                    $this->PatientList[] = array($patient_id, $blood_id, $resus);
                    return true;
                } else
                    return false;
            } else
                return false;
        } else
            return false;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPatientId()
    {
        return $this->patient_id;
    }

    /**
     * @param mixed $patient_id
     */
    public function setPatientId($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    /**
     * @return mixed
     */
    public function getBloodId()
    {
        return $this->blood_id;
    }

    /**
     * @param mixed $blood_id
     */
    public function setBloodId($blood_id)
    {
        $this->blood_id = $blood_id;
    }

    /**
     * @return mixed
     */
    public function getResus()
    {
        return $this->resus;
    }

    /**
     * @param mixed $resus
     */
    public function setResus($resus)
    {
        $this->resus = $resus;
    }

    /**
     * @return array
     */
    public function getPatientList()
    {
        return $this->PatientList;
    }

    /**
     * @param array $PatientList
     */
    public function setPatientList($PatientList)
    {
        $this->PatientList = $PatientList;
    }

}