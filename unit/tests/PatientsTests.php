<?php

require __DIR__ . "/../src/Patients.php";

use PHPUnit\Framework\TestCase;
use Patients\Patients;

class PatientsTest extends TestCase
{

    private $patients;

    public function setUp(): void
    {
        $this->patients = new patients();
    }

    public function tearDown(): void
    {
        $this->patients = NULL;
    }

    public function testOne()
    {
        $this->assertFalse($this->patients->addPatinets('Pavlo', 1, 2, '+'));
        $this->assertFalse($this->patients->addPatinets('Nastya', 2, 1, '+'));
    }

    /**
     * @param $data
     * @param $expect
     * @dataProvider addDataProvider
     */

    public function addDataProvider()
    {
        return
            [
                [['Palvo', 6, 4, '-'], false],
                [['Inna', 4, 1, '-'], true],
                [['Palvo', 3, 1, '+'], true],
                [['Palvo', 8, 4, '-'], true]
            ];
    }


}